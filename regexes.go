package main

import (
	"fmt"
	"regexp"
	"wiki"

	"github.com/pkg/errors"
)

type regexes []*regexp.Regexp

func (rs *regexes) match(bs []byte) bool {
	for _, r := range *rs {
		if r.Find(bs) != nil {
			return true
		}
	}
	// If rs contains no regexp,
	// everything matches.
	return len(*rs) == 0
}

func (rs *regexes) matchAll(set wiki.Set) bool {
	for _, e := range set {
		if rs.match(e) {
			return true
		}
	}
	return false
}

func (rs *regexes) doAppend(expr string) error {
	r, err := regexp.Compile(expr)
	if err != nil {
		return errors.Wrapf(err, "invalid regex: %s", expr)
	}
	*rs = append(*rs, r)
	return nil
}

func (rs *regexes) String() string {
	return fmt.Sprintf("%s", *rs)
}

func (rs *regexes) Set(expr string) error {
	return rs.doAppend(expr)
}
