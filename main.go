package main

import (
	"flag"
	"fmt"

	"bitbucket.org/fflo/semix/pkg/cmd"
	_ "bitbucket.org/fflo/wiki"
)

var (
	daemonHost string
	cats       = new(regexes)
)

func init() {
	flag.StringVar(&daemonHost, "daemon", "http://"+cmd.DaemonHost, "set host of semix daemon")
	flag.Var(cats, "cat", "add regex for category")
}

func main() {
	// io, err := os.Open("/mnt/ext/data/wikipedia/dewiki-20160203-pages-articles.xml.bz2")
	// ensure(err)
	// r := bzip2.NewReader(io)
	// wiki.EachArticle(r, func(a *wiki.Article) error {
	// 	if r, redirect := a.Content.Redirect(); redirect {
	// 		fmt.Printf("%s: skipping rediretion to %s\n", a.Title, r)
	// 		return nil
	// 	}
	// 	for _, art := range a.Content.Articles() {
	// 		fmt.Printf("%s: %s\n", a.Title, art)
	// 	}
	// 	return nil
	// })
	flag.Parse()
	fmt.Printf("cats: %s\n", cats)
}

func ensure(err error) {
	if err == nil {
		return
	}
	panic(err)
}
